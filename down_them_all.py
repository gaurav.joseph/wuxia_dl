#!/usr/bin/env python


import requests
import json
from bs4 import BeautifulSoup
import os
import sys
from tqdm import tqdm
import pymongo


base_path = "https://www.wuxiaworld.com"
mongo_client = None
mongo_db = None

'''
novels table
------------
{ slug, title, status, chapters, internal_status }

<novel> table (slug is the table name)
--------------------------------------
{ slug, title }
'''

def do_novels(novels):
    for novel in novels:
        request = requests.get(novel["urlpath"])
        if request.status_code == 200:
            print("Starting on novel: {0}... ".format(novel["title"]), end=' ', flush=True)
            novels_collection = mongo_db["novels"]
            novel_slug = novel["urlpath"].split("/")[-1]
            novel_document = novels_collection.find_one({"slug": novel_slug})
            soup = BeautifulSoup(request.text, features="html.parser")
            completed_tags = soup.find_all("a", href="/tag/completed")
            status = None
            if len(completed_tags) == 1:
                status = "Completed"
            else:
                status = "Ongoing"
            if novel_document == None:
                novels_collection.insert_one({"slug": novel_slug, "title": novel["title"], "status": status, "chapters": 0, "internal_status": "Ongoing"})
            else:
                if novel_document["status"] == "Completed" and novel_document["internal_status"] == "Completed":
                    print("This novel is already completed and archived.")
                    continue
                if novel_document["status"] == "Ongoing" and status == "Completed":
                    novels_collection.update_one({"slug": novel_slug}, {"$set": {"status": "Completed"}})
            os.makedirs("data/{0}".format(novel_slug), exist_ok=True)
            do_chapters("data/{0}".format(novel_slug), novel["urlpath"])
        else:
            print("Status not ok for request on url: {0}".format(novel["urlpath"]), flush=True)
            sys.exit(1)

def do_chapters(data_path, url):
    novels_collection = mongo_db["novels"]
    novel_slug = url.split("/")[-1]
    novel_document = novels_collection.find_one({"slug": novel_slug})
    request = requests.get(url)
    if request.status_code == 200:
        soup = BeautifulSoup(request.text, features="html.parser")
        chapter_containers = soup.find_all("li", class_="chapter-item")
        new_chapters = len(chapter_containers) - novel_document["chapters"]
        print("Found {0} chapters {1} new ...".format(len(chapter_containers), new_chapters), flush=True)
        if new_chapters > 0:
            novels_collection.update_one({"slug": novel_slug}, {"$set": {"internal_status": "Ongoing"}})
            novel_collection = mongo_db[novel_slug]
            for container in tqdm(chapter_containers):
                chapter_data = container.find("a")
                chapter_slug = chapter_data.get("href").split("/")[-1]
                chapter_title = chapter_data.getText().strip()
                if novel_collection.find_one({"slug": chapter_slug}) == None:
                    download_chapter("{0}/{1}.html".format(data_path, chapter_slug), "{0}{1}".format(base_path, chapter_data.get("href")))
                    novel_collection.insert_one({"slug": chapter_slug, "title": chapter_title})
            novels_collection.update_one({"slug": novel_slug}, {"$set": {"internal_status": "Completed", "chapters": len(chapter_containers)}})
    else:
        print("Failed to get data on url: {0}".format(url), flush=True)
        sys.exit(1)

def download_chapter(data_path, url):
    request = requests.get(url)
    if request.status_code == 200:
        soup = BeautifulSoup(request.text, features="html.parser")
        chapter_content = soup.find("div", id="content-container")
        with open(data_path, "w") as outfile:
            outfile.write(str(chapter_content))
    else:
        print("Failed to download chapter at url: {0}".format(url), flush=True)
        sys.exit(1)

if __name__ == "__main__":
    try:
        data = None
        mongo_client = pymongo.MongoClient("mongodb://localhost:27017/")
        mongo_db = mongo_client["wuxiaworld"]
        with open("novels.json", "r") as infile:
            data = json.load(infile)
        do_novels(data)
    except KeyboardInterrupt:
        print("User sent a quit signal.")
        print("Data already saved.")
    except pymongo.errors.PyMongoError:
        print("Mongo client had an error. Exiting.")

